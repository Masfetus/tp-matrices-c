#include <stdio.h>
#include <stdlib.h>

/* Mon doux programme, dis-moi qui est le plus beau */
int **saisie_matrice();
void free_tab_int(int **tab, int m, int n);
void show_tab_int(int **tab, int m, int n);
int **create_matrice_unite(int m);

int main(int argc, char *argv[])
{
	int **matrice, m, n;
	printf("Bienvenue dans la saisie de matrice !\n\r");	
	do
	{
		printf("Saisir un nombre de colonnes : ");
		scanf(" %d", &n);
	} 
	while (n <= 0);

	do
	{
		printf("Saisir un nombre de lignes : ");
		scanf(" %d", &m);
	} 
	while (m <= 0);
	matrice = create_matrice_unite(m);
	show_tab_int(matrice, m, m);
	free_tab_int(matrice, m, m);
	return 0;
}

int **saisie_matrice(int m, int n)
{
	int **res, **p, i;
	res = p = (int**) malloc((m + 2) * sizeof(int*));
	for(i = 0; i < m; i++, p++)
	{
		int *i_column = NULL, j = 0;
		i_column = *p = (int*) malloc(n* sizeof(int));
		for(j = 0; j < n; j++, i_column++)
		{
			printf("a(%d,%d) = ", i, j);
			scanf(" %d", i_column);
		}
	}
	return res;
}
int **create_matrice_unite(int m)
{
	int **res, **p, i;
	res = p = (int**) malloc((m + 2) * sizeof(int*));
	for(i = 0; i < m; i++, p++)
	{
		int *i_column = NULL, j = 0;
		i_column = *p = (int*) malloc((m+1) * sizeof(int));
		for(j = 0; j < m; j++, i_column++)
		{
			*i_column = 1;
		}
	}
	return res;
}
void free_tab_int(int **tab, int m, int n)
{
	int i;
	for(i = 0; i < m; i++, tab++)
	{
		int *line;
		line = *tab;
		free(line);
	}
	tab -= i;
	free(tab);
}
void show_tab_int(int **tab, int m, int n)
{
	int i, j;
	printf("|----------- Matrice -----------|\n\r");
	for(i = 0; i < m; i++, tab++)
	{
		int *line;
		line = *tab;
		printf("| ");
		for(j = 0; j < n; j++, line++)
		{
			printf("%d ", *line);
		}
		printf("|\n\r");
	}
}