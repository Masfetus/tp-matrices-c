#include <stdio.h>
#include <stdlib.h>

/* Mon doux programme, dis-moi qui est le plus beau */

int **createPascalTriangle(int lines);
void free_tab_int(int **tab);
void show_tab_pascal_int(int **tab, int lines);

int main()
{
	int **triangle_pascal = NULL, nb_lines;
	printf("Hello world !\n\r");
	do
	{
		printf("Combien de lignes au triangle ? ");
		scanf(" %d", &nb_lines);
	} 
	while (nb_lines <= 0);
	triangle_pascal = createPascalTriangle(nb_lines);
	show_tab_pascal_int(triangle_pascal, nb_lines);
	free_tab_int(triangle_pascal);
	return 0;
}

int **createPascalTriangle(int lines)
{
	int i, **res, **p, *i_line, j, *line_prec, first_try = 0;
	res = p = (int**) malloc((lines + 2) * sizeof(int*));
	for(i = 0; i < lines; i++, line_prec = *p++)
	{
		i_line = *p = (int*) malloc((i + 2) * sizeof(int));		
		for(j = 0; j < i + 1; j++, i_line++)
		{
			if(j == 0 || i == j)
				*i_line = 1;
			else
			{

				*i_line = *line_prec;
				*i_line += *++line_prec;
			}	
			printf("%03d", *i_line);
		}

		
		printf("\n");
	}
	*++p = NULL;
	return res;
}
void show_tab_pascal_int(int **tab, int lines)
{
	int i;
	printf("|----------------- Triangle de Pascal ------------------|\n\r");
	for(i = 0; i < lines; i++, tab++)
	{
		int j, *line;
		line = *tab;
		for(j = 0; j < i + 1; j++, line++)
		{
			printf("%03d ", *line);
		}
		printf("\n");
	}
}
void free_tab_int(int **tab)
{
	int i = 0;
	while(*tab != NULL)
	{
		free(*tab);
		tab++;
		i++;
	}
	tab -= i;
	free(tab);
}